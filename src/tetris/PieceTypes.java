package tetris;

public enum PieceTypes {
    I,
    J,
    L,
    O,
    S,
    T,
    Z
}

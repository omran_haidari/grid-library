package tetris;

import grid.structure.Grid;
import grid.structure.Piece;
import grid.structure.Vector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Model extends Observable implements Runnable {
    private Grid grid;
    private HashMap<Piece, PieceTypes> types;
    private ArrayList<Piece> nextPieces;
    private Piece currentPiece;
    private boolean running;
    private int score;
    
    public synchronized int getScore() {
        return score;
    }
    
    public synchronized void setScore(int score) {
        this.score = score;
    }
    
    public synchronized void addScore(int score) {
        this.score += score;
    }

    public synchronized ArrayList<Piece> getPieces() {
        return grid.getPieces();
    }

    public synchronized HashMap<Piece, PieceTypes> getTypes() {
        return types;
    }
    
    private Piece createPiece() {
        PieceTypes type = PieceFactory.pickRandomType();
        Piece piece = PieceFactory.buildPiece(new Vector(0, 0), type, grid);
        types.put(piece, type);
        return piece;
    }

    public Model() {
        grid = new Grid(new Vector(10, 22));
        types = new HashMap<>();
        nextPieces = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            nextPieces.add(createPiece());
        }
        pickPiece();
        running = false;
    }
    
    private void pickPiece() {
        checkBoard();
        currentPiece = nextPieces.get(0);
        getCurrentPiece().translationForced(new Vector(3, 0));
        nextPieces.remove(0);
        nextPieces.add(createPiece());
        grid.addPiece(getCurrentPiece());
    }
    
    public synchronized void checkBoard() {
        int nb = 0;
        for (int i = 0; i < grid.getHeight(); i++)
            if (grid.lineIsFull(i)) {
                grid.removeRow(i);
                nb++;
            }
        grid.removeEmptyPieces();
        addScore(nb * 100 * (nb / 4 + 1));
    }

    public synchronized Grid getGrid() {
        return grid;
    }
    
    public synchronized ArrayList<Piece> getNextPieces() {
        return nextPieces;
    }
    
    public synchronized Piece getCurrentPiece() {
        return currentPiece;
    }
    
    public synchronized void rotatedCurrentPiece() {
        getCurrentPiece().rotation();
    }
    
    public synchronized boolean moveCurrentPiece(int x, int y) {
        return getCurrentPiece().translationColliding(new Vector(x, y));
    }
    
    public synchronized boolean isRunning() {
        return running;
    }
    
    public synchronized void setRunning() {
        running = true;
    }
    
    public synchronized void interrupt() {
        running = false;
    }

    @Override
    public void run() {
        setRunning();
        while (isRunning()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (!moveCurrentPiece(0, 1)) {
                pickPiece();
                if (grid.pieceIsColliding(getCurrentPiece()))
                    interrupt();
            }
            setChanged();
            notifyObservers();
        }
    }
    
}

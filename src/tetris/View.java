package tetris;

import grid.graphics.GGrid;
import grid.structure.Grid;
import grid.structure.Piece;
import grid.structure.Vector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class View extends Application implements Observer {
    private GGrid gGrid;
    private Model model;
    private Stage stage;
    private Thread thread;
    
    @Override
    public void start(Stage stage) {
        this.stage = stage;
        this.stage.setOnCloseRequest(event -> close());
        showMenu();
    }
    
    public void showMenu() {
        model = null;
        Group root = new Group();
        Scene scene = new Scene(root);
        scene.setFill(Color.WHITE);
        Button startbtn = new Button("Start game!");
        startbtn.setOnAction((action) -> { startGame(); });
        startbtn.setTranslateX(100);
        startbtn.setTranslateY(120);
        root.getChildren().add(startbtn);
        stage.setWidth(300);
        stage.setHeight(300);
        stage.setScene(scene);
        stage.show();
    }
    
    public void startGame() {
        model = new Model();
        gGrid = new GGrid(model.getGrid(), 30, 1, Color.rgb(200, 200, 200));
        model.addObserver(this);
        drawScene();
        stage.setWidth(500);
        stage.setHeight(720);
        stage.show();
        thread = new Thread(model);
        thread.start();
    }
    
    public void stopGame() {
        thread.interrupt();
    }
    
    private void setControls(Scene scene) {
        scene.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
            if(key.getCode()==KeyCode.SPACE) {
                model.rotatedCurrentPiece();
                Platform.runLater(() -> notifyView());
            }
        });
        scene.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
            if(key.getCode()==KeyCode.DOWN) {
                model.moveCurrentPiece(0, 1);
                Platform.runLater(() -> notifyView());
            }
        });
        scene.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
            if(key.getCode()==KeyCode.LEFT) {
                model.moveCurrentPiece(-1, 0);
                Platform.runLater(() -> notifyView());
            }
        });
        scene.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
            if(key.getCode()==KeyCode.RIGHT) {
                model.moveCurrentPiece(1, 0);
                Platform.runLater(() -> notifyView());
            }
        });
    }
    
    private HashMap<Piece, Color> applyColors() {
        HashMap<Piece, Color> colors = new HashMap<>();
        for (Piece p : model.getPieces()) {
            colors.put(p, PieceFactory.getColor(model.getTypes().get(p)));
        }
        for (Piece p : model.getNextPieces()) {
            colors.put(p, PieceFactory.getColor(model.getTypes().get(p)));
        }
        return colors;
    }
    
    public void drawScene() {
        Group root = new Group();
        Scene scene = new Scene(root);
        scene.setFill(Color.WHITE);
        Node board = gGrid.draw(0, 0, applyColors());
        board.setTranslateX(0);
        board.setTranslateY(0);
        
        Group ui = new Group();
        Label scoreLabel = new Label("Score: " + model.getScore());
        Label nextLabel = new Label("Next pieces:");
        ui.getChildren().add(scoreLabel);
        nextLabel.setTranslateY(20);
        ui.getChildren().add(nextLabel);
        ui.setTranslateX(350);
        
        Group next = new Group();
        drawSidePanel(next);
        next.setTranslateY(100);
        ui.getChildren().add(next);
        
        root.getChildren().add(board);
        root.getChildren().add(ui);
        stage.setScene(scene);
        setControls(scene);
    }
    
    public void drawSidePanel(Group grp) {
        ArrayList<Piece> pieces = model.getNextPieces();
        for (int i = 0; i < pieces.size(); i++) {
            Piece p = pieces.get(i);
            Grid g = new Grid(new Vector(4, 4));
            g.addPiece(p);
            GGrid gg = new GGrid(g, 30, 1, Color.WHITE);
            HashMap<Piece, Color> hm = new HashMap<>();
            hm.put(p, Color.BLACK);
            Group tmp = new Group();
            tmp.getChildren().add(gg.draw(0, 0, applyColors()));
            tmp.setTranslateY(i * 4 * 30);
            grp.getChildren().add(tmp);
        }
    }

    public void close() {
        if (model != null)
            model.interrupt();
    }
    
    public void notifyView() {
        if (!model.isRunning())
            showMenu();
        else
            drawScene();
    }

    @Override
    public void update(Observable o, Object arg) {
        Platform.runLater(() -> notifyView());
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}
package grid.structure;

public interface Sizeable {
    public int getWidth();
    public int getHeight();
    public Vector getSize();
}

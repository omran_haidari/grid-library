package grid.structure;

public class Vector {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj instanceof Vector) {
            Vector v = (Vector) obj;
            if (v.getX() == x && v.getY() == y)
                return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int hash = 29;
        hash = (hash * 31) ^ x;
        hash = (hash * 31) ^ y;
        return hash;
    }

    @Override
    public String toString() {
        return "<" + x + "," + y + ">";
    }
}

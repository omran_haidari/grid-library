package grid.structure;

public class Cell implements Positionable {
    private Vector position;
    
    public Cell(Vector position) {
        this.position = position;
        
    }
       
    @Override
    public String toString() {
        return position.toString();
    }
    
    public boolean isEmpty(int x, int y) {
        return x != getX() || y != getY();
    }
    
    public void move(Vector position) {
        this.position = position;
    }

    @Override
    public int getX() {
        return position.getX();
    }

    @Override
    public int getY() {
        return position.getY();
    }

    @Override
    public Vector getPosition() {
        return position;
    }
}

package grid.structure;

import java.util.ArrayList;
import java.util.Collection;

public class Piece implements Positionable, Sizeable {
    private Cell[][] cells;
    private Vector size;
    private Vector position;
    private Grid grid;

    public synchronized Cell[][] getCells() {
        return cells;
    }

    public Piece(Vector position, Vector size, Grid grid) {
        this.position = position;
        this.size = size;
        this.grid = grid;
        cells = new Cell[getWidth()][];
        for (int i = 0; i < getWidth(); i++) {
            cells[i] = new Cell[getHeight()];
        }
    }
    
    public synchronized boolean addCell(Cell cell) {
        if (cell.getX() < 0 || cell.getY() < 0 ||
                cell.getX() >= getWidth() ||
                cell.getY() >= getHeight())
            return false;
        cells[cell.getPosition().getX()][cell.getPosition().getY()] = cell;
        return true;
    }
    
    public synchronized boolean translationColliding(Vector vector) {
        Vector nvector = new Vector(getX() + vector.getX(),
                getY() + vector.getY());
        Vector tmp = position;
        position = nvector;
        if (!grid.pieceIsColliding(this))
            return true;
        else {
            position = tmp;
            return false;
        }
    }
    
    public synchronized void translationForced(Vector vector) {
        position = new Vector(getX() + vector.getX(),
                getY() + vector.getY());
    }
    
    private synchronized void rotateMatrix(int dir) {
        Cell[][] newcells = new Cell[getHeight()][];
        for (int i = 0; i < getHeight(); i++)
            newcells[i] = new Cell[getWidth()];
        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                Cell c1 = getCellRelative(x, y);
                if (dir > 0)
                    newcells[y][getWidth() - x - 1] = c1;
                else
                    newcells[getHeight() - y - 1][x] = c1;
                if (c1 != null) {
                    if (dir > 0)
                        c1.move(new Vector(y, getWidth() - x - 1));
                    else
                        c1.move(new Vector(getHeight() - y - 1, x)); 
                }
            }
        }
        cells = newcells;
        size = new Vector(getHeight(), getWidth());
    }
    
    public synchronized boolean rotation() {
        rotateMatrix(1);
        if (grid.pieceIsColliding(this))
            rotateMatrix(-1);
        return true;
    }
    
    public synchronized void removeRow(int y) {
        Cell[][] newcells = new Cell[getWidth()][];
        for (int i = 0; i < getWidth(); i++) {
            newcells[i] = new Cell[getHeight() - 1];
            int ptr = 0;
            for (int j = 0; j < getHeight(); j++) {
                if (j != y) {
                    Cell cell = getCellRelative(i, j);
                    if (cell != null)
                        cell.move(new Vector(cell.getX(),
                                cell.getY() - (j - ptr)));
                    newcells[i][ptr] = cell;
                    ptr++;
                }
            }
        }
        cells = newcells;
        size = new Vector(getWidth(), getHeight() - 1);
        position = new Vector(getX(), getY() + 1);
    } 
    
    public synchronized boolean isEmptyRelative(int x, int y) {
        return getCellRelative(x, y) == null;
    }
    
    public synchronized boolean isEmptyGlobal(int x, int y) {
        return isEmptyRelative(x - getX(), y - getY());
    }
    
    public synchronized Cell getCellRelative(int x, int y) {
        if (x < 0 || y < 0 ||
                x >= getWidth() ||
                y >= getHeight())
            return null;
        return cells[x][y];
    }
    
    public synchronized Collection<Cell> getCellsCollection() {
        Collection<Cell> col = new ArrayList<>();
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                if (getCellRelative(i, j) != null)
                    col.add(getCellRelative(i, j));
            }
        }
        return col;
    }
    
    public synchronized Cell getCellGlobal(int x, int y) {
        return getCellRelative(x - getX(), y - getY());
    }
    
    public synchronized void setCellRelative(Cell cell, int x, int y) {
        cells[x][y] = cell;
    }
    
    public synchronized void setCellGlobal(Cell cell, int x, int y) {
        setCellRelative(cell, x - getX(), y - getY());
    }
    
    public synchronized boolean moveCellRelative(int x, int y, int nx, int ny) {
        Cell cell = getCellRelative(x, y);
        setCellRelative(cell, nx, ny);
        setCellRelative(null, x, y);
        cell.move(new Vector(nx, ny));
        return true;
    }
    
    public synchronized boolean moveCellGlobal(int x, int y, int nx, int ny) {
        return moveCellRelative(x - getX(), y - getY(), nx - getX(), ny - getY());
    }
    
    public synchronized boolean isCollidingWith(Piece p) {
        if (this.getX() < p.getX() + p.getWidth()
                && this.getX() + this.getWidth() > p.getX()
                && this.getY() < p.getY() + p.getHeight()
                && this.getY() + this.getHeight() > p.getY())
            for (Cell cell : this.getCellsCollection())
                for (Cell cell2 : p.getCellsCollection())
                    if (cell.getX() + this.getX() == cell2.getX() + p.getX()
                            && cell.getY() + this.getY() == cell2.getY() + p.getY())
                        return true;
        return false;
    }
    
    public synchronized int getCellCount() {
        int cnt = 0;
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                if (getCellRelative(i, j) != null)
                    cnt++;
            }
        }
        return cnt;
    }

    @Override
    public synchronized int getX() {
        return position.getX();
    }

    @Override
    public synchronized int getY() {
        return position.getY();
    }
    
    @Override
    public synchronized Vector getPosition() {
        return position;
    }

    @Override
    public synchronized int getWidth() {
        return size.getX();
    }

    @Override
    public synchronized int getHeight() {
        return size.getY();
    }

    @Override
    public synchronized Vector getSize() {
        return size;
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < getWidth() + 2; i++)
            s += "-";
        s += "\n";
        for (int y = 0; y < getHeight(); y++) {
            s += "|";
            for (int x = 0; x < getWidth(); x++) {
                if (getCellRelative(x, y) == null)
                    s += " ";
                else
                    s += "X";
            }
            s += "|\n";
        }
        for (int i = 0; i < getWidth() + 2; i++)
            s += "-";
        return s;
    }
}

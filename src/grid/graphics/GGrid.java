package grid.graphics;

import grid.structure.Cell;
import grid.structure.Grid;
import grid.structure.Piece;
import java.util.HashMap;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;

public class GGrid {
    private Grid grid;
    private GCell[][] matrix;
    private int size, offset;
    private Color background;

    public GGrid(Grid grid, int size, int offset, Color background) {
        this.grid = grid;
        this.size = size;
        this.offset = offset;
        this.background = background;
        matrix = new GCell[grid.getWidth()][];
        for (int i = 0; i < grid.getWidth(); i++) {
            matrix[i] = new GCell[grid.getHeight()];
            for (int j = 0; j < grid.getHeight(); j++)
                matrix[i][j] = new GCell(background);
        }
    }

    public Node draw(int x, int y, HashMap<Piece, Color> colors)
    {
        Group group = new Group();
        for (int i = 0; i < matrix.length; i++)
            for (int j = 0; j < matrix[i].length; j++)
                group.getChildren().add(
                        matrix[i][j].draw(i * size + i * offset + x,
                                j * size + j * offset + y, size));
        for (Piece piece : grid.getPieces()) {
            int xp = piece.getX();
            int yp = piece.getY();
            for (Cell cell : piece.getCellsCollection()) {
                int xc = xp + cell.getX();
                int yc = yp + cell.getY();
                group.getChildren().add(
                        new GCell(colors.get(piece)).draw(xc * size + xc * offset,
                                yc * size + yc * offset, size));
            }
        }
        return group;
    }
}

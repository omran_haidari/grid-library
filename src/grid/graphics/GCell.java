package grid.graphics;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class GCell {
    private Color color;
    
    public GCell(Color color) {
        this.color = color;
    }

    public final Node draw(int x, int y, int size) {
        Group group = new Group();
        Rectangle rec = new Rectangle(x, y, size, size);
        rec.setFill(color);
        group.getChildren().add(rec);
        return group;
    }
}
